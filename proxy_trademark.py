import click
from flask import Flask
import requests
from bs4 import BeautifulSoup


@click.command()
@click.option('--host', default='127.4.6.1', help='HOST')
@click.option('--port', default=7000, help='PORT')
@click.option('--site', default='https://medium.com', help='default website to visit')
@click.option('--ln', default='10', help='length parameter to replace a word')
@click.option('--symbol', default='™', help='trademark symbol to replace chosen words')
def variable_addition(host, port, site, ln, symbol):
    global ADDRESS, PORT, SITE, LEN, SYM
    ADDRESS = host
    PORT = port
    SITE = site
    LEN = int(ln)
    SYM = symbol
    main(ADDRESS, PORT, SITE, LEN, SYM)


def main(ADDRESS, PORT, SITE, LEN, SYM):
    app = Flask(__name__)
    app.config.from_object(__name__)

    @app.route('/', defaults={'path': SITE})
    @app.route('/<path:path>')
    def proxy(path):
        strings = requests.get(f'{path}').content
        return change(strings)

    def change(data):
        soup = BeautifulSoup(data, 'html.parser')
        all = soup.find('body').find_all(text=True)
        for part in all:
            changed_text = part.strip().split()
            changed_text = [i + SYM if len(i) >= LEN else i for i in changed_text]
            part.replace_with(' '.join(changed_text))
        return soup.prettify(formatter=None)

    app.run(host=ADDRESS, port=PORT)


variable_addition()
